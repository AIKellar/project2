import React from 'react'
import {Card, Form, Button} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import AllPostsContainer from '../ContainerComponents/AllPostsContainer'
import AllUserContainers from '../ContainerComponents/AllUsersContainer'
import { useSelector, useDispatch} from 'react-redux';

 function HomePage() {

        const Username = useSelector(state => state.Username);
        console.log(Username)


    return (
        
            
        <>
        <AllUserContainers></AllUserContainers>
        <AllPostsContainer></AllPostsContainer>
        </>
        
     
    )
}

export default HomePage;