import React from 'react'
import {Navbar, Nav, NavDropdown, Form, Button, FormControl} from 'react-bootstrap'
import {Redirect, Link} from 'react-router-dom';
import { useSelector, useDispatch} from 'react-redux';
import { SearchBar} from '../actions/index';
import axios from "axios";
import { GetSpecificUserPosts } from '../actions';
import { useHistory } from 'react-router-dom';



function HeaderUser() {

  function OurLogout(){
    localStorage.clear('UserLocal');
    if(window.location.href!=="http://localhost:3000/"){
         <Redirect  to="/" />
         console.log("hi")
    }else{
       
    window.location.reload();
    }
}

const dispatch = useDispatch();

const Search = useSelector(state => state.SearchBar)
const nameUsers = useSelector(state => state.UserList)
const history = useHistory()

function SearchClick() {
  console.log(Search);
  axios.post('http://localhost:9005/Project2/socialmedia/getPostByUsername',{
    userName: Search
  })
  .then((response)=>{
    dispatch(GetSpecificUserPosts(response.data))
  })
  
}

function RedirectToProfilePage() {
  
  nameUsers.forEach(nameUsers =>{
      console.log("Hello");
      history.push("/OtherUser")
      
      
    
    // else{
    //   history.push("/UserSearchNotFound")
    // }

  })


  

  

}


    return (    
      <div className="">
        <Navbar bg="dark" expand="lg" >
  <Navbar.Brand id ="NavbarLinks">Social Turtle</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav" className="">
  
    <Nav className="mr-auto">
    <Nav.Link>  <Link to="/" id ="NavbarLinks">Home</Link> </Nav.Link> 
    <Nav.Link><Link href="#link" id ="NavbarLinks" style = {{margin: "4px"}}>Link</Link > </Nav.Link> 
      <NavDropdown title="Profile Action" id="basic-nav-dropdown" >
       <Nav.Link> <Link to="/UserUpdate" id ="dropdown">Update User info</Link> </Nav.Link>
       <Nav.Link> <Link to="/UpdatePassword" id ="dropdown">Change Password</Link> </Nav.Link>
       <Nav.Link> <Link to="/ProfilePic" id ="dropdown">Change Picture</Link> </Nav.Link>
       <Nav.Link> <Link to="/MyProfile" id ="dropdown">My Profile</Link> </Nav.Link>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#Post">Make a Post</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    

 
    <Form inline className="mr-auto">
      <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={(e) => dispatch(SearchBar(e.target.value))} />
      <Button variant="outline-success" className="no-button-margin" id="navbar-search-button" onClick={RedirectToProfilePage}
      >Search</Button>
      
      
        
        

    </Form>


 
    <Button variant='outline-danger' onClick={OurLogout} className="no-button-margin" id="navbar-logout">
          <a className="text-danger nounderline" href="/">
            Logout
            </a>
        </Button>

    </Navbar.Collapse>
</Navbar>
</div>
    )
}

export default HeaderUser;