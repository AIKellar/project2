import React from 'react'
import {Card, Form, Button} from 'react-bootstrap';
import { useSelector, useDispatch} from 'react-redux';
import { updatePassWord } from '../actions/index';
import emailjs from 'emailjs-com';
import axios from 'axios';


 function ChangePassword() {

    const id = useSelector(state => state.LocalUser.userId);
    const Username = useSelector(state => state.Username);
    const Paswordo = useSelector(state => state.Password);
    const firstName = useSelector(state => state.FirstName);
    const lastName = useSelector(state => state.LastName);
    const Emailo = useSelector(state => state.Email);

    const dispatch = useDispatch();


    function sendEmail(){

        console.log("Hello")

        emailjs.send("gmailProject2","MyReactTemplate",{
            subject: "Updating Pasword",
            name: "Social turtle network",
            email: "@SocialTurtle Media inc",
            message: "Updated password to " + Paswordo,
            }, "user_W4ichUtq85FZ5Jk2G1svH")
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
            console.log(id)


         axios.post('http://localhost:9005/Project2/socialmedia/updateUserInfo',{
             userName : Username,
             passWord : Paswordo,
             firstname : firstName,
             lastname : lastName,
             email : Emailo, 
             userId : id
          })

    }

    return (
        <div className='d-flex justify-content-center'>
        <form>
            <Card style={{ width: '25rem' }}>
            <Form.Group controlId="formBasicPassword">
<Form.Label> Update Password</Form.Label>
<Form.Control type="password" placeholder="Password" onChange={(e) => dispatch(updatePassWord(e.target.value))} />
</Form.Group>


                </Card>

                <Button onClick={sendEmail}> Update Credentials</Button>

                </form>
                </div>
       
    )
}

export default ChangePassword;
