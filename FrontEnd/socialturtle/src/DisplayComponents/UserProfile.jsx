import React, {useState} from 'react';
import {  MDBRow,  MDBCard, MDBCardBody, MDBIcon, MDBCol, MDBCardTitle, MDBBtn} from "mdbreact";
import { useSelector, useDispatch} from 'react-redux';
import { uploadFile } from 'react-s3';
import S3FileUpload from 'react-s3';
import axios from "axios";

const UserProfile = () => {
    const [selectedFile, setSelectedFile] = useState(null);
    let key = ''
    let uploadUrl = ''

    const config = {
        bucketName: 'https://s3uploader-s3uploadbucket-1kxorci6fo217.s3.amazonaws.com/6788525.jpg?AWSAccessKeyId=ASIAYUGLMRZFJFAUKZ4Z&Content-Type=image%2Fjpeg&Expires=1610842915&Signature=a8O7ipBrHg6VoJXSlLxkUNiyHpI%3D&x-amz-security-token=IQoJb3JpZ2luX2VjEGEaCXVzLWVhc3QtMSJHMEUCIQDoqkEx8yXuQ9UqaJ%2BBKrZq0A8ZF936%2FowFDAPqONUu2wIgY1P4yRPjOLL2q0XVGProrelC27txKJm8vhALA9l173Uq7AEIORAAGgw1OTMxMzIwMzE1NjIiDCFAs2b5JEEvQw6MFSrJAWMGLyn7uLjT6ZQMuiZZrmslY6hbnYhCbL48R6OKR8ILpxwAN3uU4Ab70SBPojl5zZY6qJ%2FqJgxdR5U%2BEyoK3VZ6ybJLRefStck6j7Px6Sx33cTobagBZ%2BU1uv2StvFTs%2BHMAsc2Bw48lHGeBfCVF%2B7IZ2jZ7fcFxvC4M2seS4cUAwuL6sau4hmK%2FxAcS9%2B%2FSMKuHLNq5g%2FKMS2H6xbDU%2FZVZf6jPgGRn1KtWXVl1OU4udsIm2JZ%2FnjmSDiaoI0UAzZxGPaQygjRXjD2g46ABjrgAQaRwgRsy8QRlon4sucaLcN2PtoWwQ%2Fu6v3MwH4TpGgTp0SoVyM5r3aI3oR3lmeq94JF7zBPVBxNMD9unuNiXOwcDH63cZEmR4YFrtsWlfZSdpP%2BkAaLjvFx7oI86E9c3G8PUzlDYC5g5H15L0iYgojXPK%2Fq%2BNCvjrsLgc%2Fb7w7r35PefrzQ7uSCXf4mJJEdC%2BG0o3R%2BfJhMAl4GwiUckew7XL66WNPE4SIJU8mKTpQQqm6sGMDf22O9AJSj5yltw9LtMHourYFTUHnwU5G5zZ71BeAZj2hlFSRq80cFoZ6M',
        dirName: 'photos', /* optional */
        region: 'us-east-1',
        accessKeyId: 'AKIAYUGLMRZFFMRSDGHS',
        secretAccessKey: '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',
    }

    const user = useSelector(state => state.LocalUser);

    function uploadPicture(e) {
        console.log(e.target.files[0])
        // uploadFile( e.target.files[0], config)
        // .then( (data)=> {
        //     console.log(data);
        // })
        // .catch( (err)=>{
        //    alert(err) 
        // })
      axios.get('https://9ztixggtca.execute-api.us-east-1.amazonaws.com/uploads')
      /// We have it in local storage now -Andrew
      .then(function (response) {
        console.log(response.data);
        //const User = response.data
        key = response.data.Key;
        uploadUrl = response.data.uploadURL;
        }
      )
      .then(function () {
        axios.put(uploadUrl)
      })
      .catch(err => console.error(err))
    }
    return (
        <div>   
             <MDBCard
          className='card-image' id='profile-background'
          style={{
            backgroundImage:
              "url('https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2814%29.jpg')",
            objectFit: "cover"
          }}
        >
          <div className='text-white text-center d-flex justify-content-center rgba-black-strong py-5 px-4'>
            <div>
              <h5 className='pink-text'>
                <MDBIcon icon='chart-pie' /> {user.userName}
              </h5>
              <MDBCardTitle tag='h3' className='pt-2'>
                <strong>{user.firstname} {user.lastname}</strong>
              </MDBCardTitle>
              
              <MDBBtn color='pink'>
              <form>
                    <input
                    type="file"
                    value={selectedFile}
                    onChange={uploadPicture}
                    />
            </form>
                
              </MDBBtn>
            </div>
          </div>
          
        </MDBCard>
        </div>
    );
};

export default UserProfile;