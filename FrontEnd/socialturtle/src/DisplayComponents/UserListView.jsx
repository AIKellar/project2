import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch} from 'react-redux';
import {ListGroup} from 'react-bootstrap'

function UserListView() {

    const allUsers = useSelector(state => state.UserList);


    return (
        <div>
            <h4>User List </h4>
            {allUsers.map(names=>{
                return(
<ListGroup key = {names}>
<ListGroup.Item>{names}</ListGroup.Item>
</ListGroup>


             ) })}
           
        </div>
    )
}

export default UserListView;