import React, {useState} from 'react';
import {  MDBRow,  MDBCard, MDBCardBody, MDBIcon, MDBCol, MDBCardImage, MDBInput} from "mdbreact";
import LikeButton from './LikeButton'

const AllPosts = (props) => {

    const[clickedLikes, setClickedLikes] = useState(0);

    function insertLike(likes) {
        console.log(likes)
       // setClickedLikes(likes)

    }
    console.log(props.posts);
    if(props.posts && props.posts.length) {
        return <div>
        {props.posts.map((post)=>{

        return (
            
            <MDBRow className="d-flex justify-content-center">
            <MDBCol md="6" lg="4">
              <MDBCard news className="my-5">
                <MDBCardBody>
                  <div className="content">
                    <div className="right-side-meta">14 h</div>
                    <img
                      src="https://mdbootstrap.com/img/Photos/Avatars/img%20(17)-mini.jpg"
                      alt=""
                      className="rounded-circle avatar-img z-depth-1-half"
                    />
                    {post.author.userName}
                  </div>
                </MDBCardBody>
                <MDBCardImage
                  top
                  src="https://mdbootstrap.com/img/Photos/Others/girl1.jpg"
                  alt=""
                />
                <MDBCardBody>
                  <div className="social-meta">
                    <p>{post.description} </p>
                    <span>
                      <LikeButton likes={post.likes.length} postId={post.postId} postLikes={post.likes}></LikeButton>
                    </span>
                    <p>
                      <MDBIcon icon="comment" />
                      13 comments
                    </p>
                  </div>
                  <hr />
                  <MDBInput far icon="heart" hint="Add Comment..." />
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </MDBRow>

        
        )})}
        </div>
    }
   else {
    console.log("no data");   
    return (
        <div>
            <h1>Hello</h1>
        </div>
    );
   }
};

export default AllPosts;
